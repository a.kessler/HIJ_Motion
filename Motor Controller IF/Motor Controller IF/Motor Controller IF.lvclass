﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Motor Controller IF.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../Motor Controller IF.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.IsInterface" Type="Bool">true</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="Controller Info Text.vi" Type="VI" URL="../Controller Info Text.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'4!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!-0````]+47^U&lt;X)A4G&amp;N:1!!%E!Q`````QF*&lt;G:P)&amp;2F?(1!7%"Q!"Y!!$=:47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;GRJ9BN.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F',GRW9WRB=X-!&amp;EVP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!#%!!!!B!!!!#1!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
	<Item Name="Controller renamed.vi" Type="VI" URL="../Controller renamed.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'2!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!-0````])4G6X)%ZB&lt;75!!"*!-0````])4WRE)%ZB&lt;75!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!":.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!B!!!!)1!!!!E!!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
	<Item Name="Motor Info Text.vi" Type="VI" URL="../Motor Info Text.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'4!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!-0````]*37ZG&lt;S"5:8BU!"2!-0````]+47^U&lt;X)A4G&amp;N:1!!7%"Q!"Y!!$=:47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;GRJ9BN.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F',GRW9WRB=X-!&amp;EVP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!#%!!!!B!!!!#1!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685376</Property>
	</Item>
	<Item Name="Motor renamed.vi" Type="VI" URL="../Motor renamed.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'2!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!-0````])4G6X)%ZB&lt;75!!"*!-0````])4WRE)%ZB&lt;75!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!":.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!B!!!!)1!!!!E!!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741832</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
	</Item>
	<Item Name="Publish Error.vi" Type="VI" URL="../Publish Error.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!':!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!-0````]+47^U&lt;X)A4G&amp;N:1!!'%"1!!-!!!!"!!),47^U&lt;X)A28*S&lt;X)!7%"Q!"Y!!$=:47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;GRJ9BN.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F',GRW9WRB=X-!&amp;EVP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!#%!!!!"!!!!#1!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685376</Property>
	</Item>
	<Item Name="Publish HW Config.vi" Type="VI" URL="../Publish HW Config.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'0!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!-0````]+47^U&lt;X)A4G&amp;N:1!!$E"4#5B8)%.P&lt;G:J:Q"91(!!(A!!.RF.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F',GRW&lt;'FC'UVP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:D&lt;'&amp;T=Q!747^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2C"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!)!!E#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!)1!!!!%!!!!*!!!!!!!1!+!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685376</Property>
	</Item>
	<Item Name="Publish Motor State.vi" Type="VI" URL="../Publish Motor State.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!',!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!-0````]+47^U&lt;X)A4G&amp;N:1!!#E"4"6.U982F!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!":.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!B!!!!!1!!!!E!!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685376</Property>
	</Item>
	<Item Name="Publish Position.vi" Type="VI" URL="../Publish Position.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'1!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!^!#A!)5'^T;82J&lt;WY!!"2!-0````]+47^U&lt;X)A4G&amp;N:1!!7%"Q!"Y!!$=:47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;GRJ9BN.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F',GRW9WRB=X-!&amp;EVP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359A;7Y!!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!1!!!!#!!!!B!!!!#1!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685376</Property>
	</Item>
	<Item Name="Register Controller.vi" Type="VI" URL="../Register Controller.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(&amp;!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%J!=!!?!!!Q&amp;5&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9BB.:8.T97&gt;F)%6O=86F&gt;76S,GRW9WRB=X-!!!Z$&lt;WZU=G^M&lt;'6S)%6O=1!!$E!Q`````Q2/97VF!!"91(!!(A!!.RF.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F',GRW&lt;'FC'UVP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:D&lt;'&amp;T=Q!747^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2C"J&lt;A!!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"Q!)!!E#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!1!!!#%!!!!*!!!!!!!1!+!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685376</Property>
	</Item>
	<Item Name="Register Motor.vi" Type="VI" URL="../Register Motor.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(.!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!"&gt;.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'^V&gt;!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!%J!=!!?!!!Q&amp;5&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9BB.:8.T97&gt;F)%6O=86F&gt;76S,GRW9WRB=X-!!!Z$&lt;WZU=G^M&lt;'6S)%6O=1!!&amp;E!Q`````QR-&lt;W&gt;J9W&amp;M)%ZB&lt;75!!&amp;B!=!!?!!!X'5VP&gt;'^S)%.P&lt;H2S&lt;WRM:8)A359O&lt;(:M;7)&lt;47^U&lt;X)A1W^O&gt;(*P&lt;'RF=C"*2CZM&gt;G.M98.T!":.&lt;X2P=C"$&lt;WZU=G^M&lt;'6S)%F')'FO!!"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!A!!!))!!!!E!!!!!!"!!I!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1115685376</Property>
	</Item>
</LVClass>
