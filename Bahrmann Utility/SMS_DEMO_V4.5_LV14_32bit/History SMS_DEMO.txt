NET_DEMO History

   Date		Change

2013.04.03	error handling case, added read/save automatic/manual motorsetting
		added error handling with wrong motor
		added de-/aktivation from the contol-buttons
2013.04.10	added current-error popup,some little errors fixed
		added error(-1)->load default parameter(doesn't exist'ini'or not ok)
2013.08.02	extra window for fullsteps and micrsteps; added 16,32 microstep for "MODOUL-SMS"
		adjusted following subroutines: NET_CMD,NET_DAT
		added subroutine NET_MIC_STRING ->represent fracture for microsteps
2013.09.18	remove new subrutine NET_MIC_SOFT ->old was better
2013.10.14	added description (info)
		adjusted in NET-CMD the command "get status short"->new: return all "Status-Bits"
2014.04.28	new surface(goto Button,action window....
		updated and optimated many subroutines
		new routines: intit (NET_VER_CHECK), extended command(NET_EX_CMD), extended data(NET_EX_DAT)
		a part of the funtion in the new subroutines is passwort protected(only for administrators)
		with the new software can you use the new and old interface protocols(selected by firmware version)
2014.06.27	fix error in USB-Demo.vi, added 'old SMS(1689)' to useable devices(VER_CHECK.vi)
2014.07.18	fix error in USB-Demo.vi  added	'old SMS(1725)' to useable devices(VER_CHECK.vi)
2015.02.24	fix error in NET_VER_CHECK change the serial(4K19->MSK0)
2016.01.12	fix error in SEND_CMD by command "get settings" ->return stepwidth 1/16,1/32 now displayed correct
version 3.5
2016.02.29	use USB&NET in 'one for all Tool', rename all sub VI for USB&NET ->USB_NET...
		split the Libary's in ALLXX.llb SMSXX.llb, fix problems by auto/manualy load Parameters
2016.03.02	fix required connections by many VI
2016.07.21	added new firmware-id to USB_NET_VER_CHECK.vi
2016.08.17	added new command(goto parkposition) to USB_NET_CMD.vi, fix a problem with USB_NET_MIC_STRING.vi
2017.01.16	added new commands(get saved position, get saved steps, goto postion, goto saved position run saved steps+/-) to USB_NET_CMD.vi
		added new commands(position&run, save position, save steps, store steps, store position) to USB_NET_EDAT_SMS.vi
		rename some action-name, remove "lock"-input --> USB_NET_EDAT_SMS.vi
		added new version-identification to USB_NET_VER_CHECK->value as integer
		added new parameters(position/steps/micro/soft) to read/write the ini-file
		added new commands to Demo(goto parkposition,run saved steps(+/-), goto saved position)
		added a new register(extra) for global adjustment& programm settings
		-->global bits read-&writeable, stop at "motor change" adjustable ->Several motors can be used simultaneously 
		added version detection-->enable/disabele Buttons & functions
		added expanded parameters for steps,position,special(miro/soft-bits)->read/wite
		added possibility store permanently ->softlimits,name,position,steps,movement
		with the firmware(code 2) it is possible store position,steps,movement over network
		fix many errors and optimated programm
2017.02.01	fix error by save global adjustment,store parameters with network, added pop-up window after writting adjustment,parameters
2017.02.14	fixed a error and added password for special-set in USB_NET_EDAT_SMS.vi, password was added to USB-DEMO.vi for special-set
2017.03.07	fixed sign error by set/save position in USB_NET_EDAT_SMS.vi
2017.03.15	created new library with subroutines from demo DEMOxxxx.llb ->not longer in SMSxxx.llb library
2017.12.12	fixed error by set/clear softlimits, added new Sub-VI's->USB_NET_KONF_SMS,remove older VI->USB_NET_EDAT_SMS
		expand USB_NET_CMD_SMS -> run and goto function usable for all version, added errorcode -3 for analysis older firmware
		implement vcode in DEMO-SMS and SUB_VI's, rename variables and optimize programm
2018.05.03	fixed errors when global bits set/clear, added older devices to USB_NET_VER_CHECK ->SMS-2,SMS-3,SMS-5,Z-Modul
2018.11.27	added automatic interface speed(57600/9600) for USB ->USB_INIT(ALLxxx.LLB)
2020.06.11	added a part of VC-3, fixed errors
2020.12.12	createt new setup for set parameters, included id-chip, fixed many errors
2020.01.25	fixed error by statusbyte in USB_NET_SMS_CMD set bit-names to "standard"
