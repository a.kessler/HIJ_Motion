﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="Abstract Messages for Caller" Type="Folder"/>
	<Item Name="Messages for this Actor" Type="Folder">
		<Item Name="After Launch Init Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/After Launch Init Msg/After Launch Init Msg.lvclass"/>
		<Item Name="Build Complete Menu Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Build Complete Menu Msg/Build Complete Menu Msg.lvclass"/>
		<Item Name="Build Error Handling Menu Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Build Error Handling Menu Msg/Build Error Handling Menu Msg.lvclass"/>
		<Item Name="Build GoTo Menu Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Build GoTo Menu Msg/Build GoTo Menu Msg.lvclass"/>
		<Item Name="GoTo EC Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/GoTo EC Msg/GoTo EC Msg.lvclass"/>
		<Item Name="Move Absolut Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Move Absolut Msg/Move Absolut Msg.lvclass"/>
		<Item Name="Move Relative Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Move Relative Msg/Move Relative Msg.lvclass"/>
		<Item Name="New Motion State Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/New Motion State Msg/New Motion State Msg.lvclass"/>
		<Item Name="New Motor Error Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/New Motor Error Msg/New Motor Error Msg.lvclass"/>
		<Item Name="New Position Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/New Position Msg/New Position Msg.lvclass"/>
		<Item Name="Propose new Position Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Propose new Position Msg/Propose new Position Msg.lvclass"/>
		<Item Name="Read UI Label Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Read UI Label Msg/Read UI Label Msg.lvclass"/>
		<Item Name="Set State of Controls Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Set State of Controls Msg/Set State of Controls Msg.lvclass"/>
		<Item Name="Stop Movement Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Stop Movement Msg/Stop Movement Msg.lvclass"/>
		<Item Name="Substitute Motor UI Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Substitute Motor UI Msg/Substitute Motor UI Msg.lvclass"/>
		<Item Name="Write Controller Info Text Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Write Controller Info Text Msg/Write Controller Info Text Msg.lvclass"/>
		<Item Name="Write Motor Info Text Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Write Info Text Msg/Write Motor Info Text Msg.lvclass"/>
		<Item Name="Write User_s last Position Msg.lvclass" Type="LVClass" URL="../Motor UI Base Messages/Write User_s last Position Msg/Write User_s last Position Msg.lvclass"/>
	</Item>
	<Item Name="Motor UI Base.lvclass" Type="LVClass" URL="../Motor UI Base/Motor UI Base.lvclass"/>
</Library>
