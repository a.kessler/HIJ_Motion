﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2014_data</Property>
	<Property Name="Alarm Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2014\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">C__Program_Files__x86__National_Instruments_LabVIEW_2014_data</Property>
	<Property Name="Database Path" Type="Str">C:\Program Files (x86)\National Instruments\LabVIEW 2014\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">true</Property>
	<Property Name="Enable Data Logging" Type="Bool">true</Property>
	<Property Name="NI.Lib.FriendGUID" Type="Str">88190b28-0d82-492f-a4a0-25ce32f92fea</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="Friends List" Type="Friends List">
		<Item Name="Motor UI Base.lvlib" Type="Friended Library" URL="../../Motor UI Base/Motor UI Base.lvlib"/>
	</Item>
	<Item Name="Abstract Messages for Caller" Type="Folder"/>
	<Item Name="Messages for this Actor" Type="Folder">
		<Property Name="NI.SortType" Type="Int">0</Property>
		<Item Name="After Launch Init Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/After Launch Init Msg/After Launch Init Msg.lvclass"/>
		<Item Name="Connect Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Connect Msg/Connect Msg.lvclass"/>
		<Item Name="Connect UI Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Connect UI Msg/Connect UI Msg.lvclass"/>
		<Item Name="Disconnect Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Disconnect Msg/Disconnect Msg.lvclass"/>
		<Item Name="Get Controller Name Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Get Controller Name Msg/Get Controller Name Msg.lvclass"/>
		<Item Name="Get Motor Names Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Get Motors Log. Names Msg/Get Motor Names Msg.lvclass"/>
		<Item Name="Is Controller error Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Is SMS global error Msg/Is Controller error Msg.lvclass"/>
		<Item Name="Motor Movement is Complete Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Motor Movement is Complete Msg/Motor Movement is Complete Msg.lvclass"/>
		<Item Name="Move Axis Aboslute Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Move Axis Aboslute Msg/Move Axis Aboslute Msg.lvclass"/>
		<Item Name="Move Axis Relative Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Move Axis Relative Msg/Move Axis Relative Msg.lvclass"/>
		<Item Name="Move Axis to EC Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Move Axis to EC Msg/Move Axis to EC Msg.lvclass"/>
		<Item Name="Open Config Dialog Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Open Config Dialog Msg/Open Config Dialog Msg.lvclass"/>
		<Item Name="Open Maintenance UI Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Open Maintenance UI Msg/Open Maintenance UI Msg.lvclass"/>
		<Item Name="Open Motor Config Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Open Motor Config Msg/Open Motor Config Msg.lvclass"/>
		<Item Name="Reset Error Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Reset Error Msg/Reset Error Msg.lvclass"/>
		<Item Name="Return LVClass Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Return LVClass Msg/Return LVClass Msg.lvclass"/>
		<Item Name="Stop Axis Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Stop Axis Msg/Stop Axis Msg.lvclass"/>
		<Item Name="Update Axis State Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Update Axis State Msg/Update Axis State Msg.lvclass"/>
		<Item Name="Write Controller Settings to Config Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Write Settings to Config Msg/Write Controller Settings to Config Msg.lvclass"/>
		<Item Name="Zero Counter Msg.lvclass" Type="LVClass" URL="../Motor Controller Actor Messages/Zero Counter Msg/Zero Counter Msg.lvclass"/>
	</Item>
	<Item Name="Typedefs" Type="Folder">
		<Item Name="Controller Settings.ctl" Type="VI" URL="../Motor Controller Actor/Controller Settings.ctl"/>
		<Item Name="Controller List.ctl" Type="VI" URL="../Controller List.ctl"/>
		<Item Name="Connection State.ctl" Type="VI" URL="../Motor Controller Actor/Connection State.ctl"/>
	</Item>
	<Item Name="Motor Controller Actor.lvclass" Type="LVClass" URL="../Motor Controller Actor/Motor Controller Actor.lvclass"/>
</Library>
