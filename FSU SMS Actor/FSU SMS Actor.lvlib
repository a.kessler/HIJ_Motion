﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Abstract Messages for Caller" Type="Folder"/>
	<Item Name="Messages for this Actor" Type="Folder">
		<Item Name="Write Connection Settings Msg.lvclass" Type="LVClass" URL="../FSU SMS Actor/FSU SMS Actor Messages/Write SMS Settings Msg/Write Connection Settings Msg.lvclass"/>
		<Item Name="Update Motor Obj Cnfg Settings Msg.lvclass" Type="LVClass" URL="../FSU SMS Actor/FSU SMS Actor Messages/Update Motor Obj Cnfg Settings Msg/Update Motor Obj Cnfg Settings Msg.lvclass"/>
	</Item>
	<Item Name="Typedefs" Type="Folder">
		<Item Name="SMS Description.ctl" Type="VI" URL="../FSU SMS Actor/FSU SMS Actor/SMS Description.ctl"/>
		<Item Name="InitParams.ctl" Type="VI" URL="../../Motor Actor/Motor Actor/InitParams.ctl"/>
		<Item Name="Motor Enum.ctl" Type="VI" URL="../../Motor Actor/Motor Actor/Motor Enum.ctl"/>
		<Item Name="Motor HW Cnfg.ctl" Type="VI" URL="../../Motor Actor/Motor Actor/Motor HW Cnfg.ctl"/>
		<Item Name="Speed Cnfg.ctl" Type="VI" URL="../../Motor Actor/Motor Actor/Speed Cnfg.ctl"/>
		<Item Name="Motor State.ctl" Type="VI" URL="../../Motor Actor/Motor Actor/Motor State.ctl"/>
		<Item Name="Th Bahrmann Error codes.ctl" Type="VI" URL="../../Motor Actor/Motor Actor/Th Bahrmann Error codes.ctl"/>
	</Item>
	<Item Name="Errors" Type="Folder">
		<Item Name="FSU Motor Controller-errors.txt" Type="Document" URL="../../Error Handling/FSU Motor Controller-errors.txt"/>
	</Item>
	<Item Name="FSU SMS Actor.lvclass" Type="LVClass" URL="../FSU SMS Actor/FSU SMS Actor/FSU SMS Actor.lvclass"/>
	<Item Name="FSU SMS Device.ico" Type="Document" URL="../FSU SMS Device.ico"/>
</Library>
